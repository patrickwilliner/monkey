# Monkey SVG editor

Very simple SVG editor written in Javascript using angular and d3.

Demo app can be found here: [https://monkey-svg-eu.herokuapp.com/](https://monkey-svg-eu.herokuapp.com/)
