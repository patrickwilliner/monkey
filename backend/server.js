'use strict';

var express      = require('express');
var http         = require('http');
var path         = require('path');

var port     = process.env.PORT || 3000;
var ip       = process.env.IP || 'localhost';

var app = express();

// set up express application
app.set('port', port);
app.set('view engine', 'html'); // set up html for templating
app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/../app/views');
app.use(express.static(path.join(__dirname, '/../app')));

require('./config/routes.js')(app);

var server = http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + ip + ':' + server.address().port);
});