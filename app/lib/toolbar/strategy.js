var toolbar = (function(ns) {
    "use strict";

	function Strategy() {	
	}

	Strategy.prototype.onMouseClick = function(scope, x, y) {
	};

    Strategy.prototype.onDragStart = function(scope, x, y) {
    };

    Strategy.prototype.onDrag = function(scope, x, y) {
    };

    Strategy.prototype.onDragEnd = function(scope, x, y) {
    };

    Strategy.prototype.backgroundColorChanged = function(scope) {
    };

	// exports 
	ns.Strategy = Strategy;

	return ns;
})(toolbar);