var toolbar = (function(ns) {
    "use strict";

	function SelectionStrategy() {
        this.dragComponent = null;
        this.ox = null;
        this.oy = null;
	}

	// set up inheritance
	SelectionStrategy.prototype = new ns.Strategy();
	SelectionStrategy.prototype.constructor = SelectionStrategy;

	SelectionStrategy.prototype.onMouseClick = function(scope, x, y) {
        var component = null,
            idx;

        for (idx = 0; idx < scope.shapeList.size(); idx++) {
            if (scope.shapeList.get(idx).contains(x, y)) {
                component = scope.shapeList.get(idx);
                break;
            }
        }

        unselectAll(scope);

        if (component && !(component instanceof draw.Selection)) {
            var bb = component.getBoundingBox(),
                selection = new draw.Selection(bb.x, bb.y, bb.h, bb.w);

            selection.addComponent(component);
            scope.shapeList.set(idx, selection);
        }

        scope.render();
	}

    SelectionStrategy.prototype.onDragStart = function(scope, x, y) {
        var component = null,
            idx;

        for (idx = 0; idx < scope.shapeList.size(); idx++) {
            if (scope.shapeList.get(idx).contains(x, y) && scope.shapeList.get(idx) instanceof draw.Selection) {
                component = scope.shapeList.get(idx);
                break;
            }
        }

        this.dragComponent = component;
        this.ox = x;
        this.oy = y;
    }

    SelectionStrategy.prototype.onDrag = function(scope, x, y) {
        if (this.dragComponent) {
            var dx = x - this.ox,
                dy = y - this.oy;

            this.dragComponent.translate(dx, dy);
            this.ox = x;
            this.oy = y;
            scope.render();
        }
    }

    SelectionStrategy.prototype.onDragEnd = function(scope, x, y) {
        this.dragComponent = null;
        this.ox = null;
        this.oy = null;
    }

    SelectionStrategy.prototype.backgroundColorChanged = function(scope) {
        var i;

        for (i = 0; i < scope.shapeList.size(); i++) {
            if (scope.shapeList.get(i) instanceof draw.Selection) {
                scope.shapeList.get(i).setBackgroundColor(scope.bgColor);
            }
        }

        scope.render();
    }

    function unselectAll(scope) {
        var i;

        for (i = 0; i < scope.shapeList.size(); i++) {
            if (scope.shapeList.get(i) instanceof draw.Selection) {
                scope.shapeList.set(i, scope.shapeList.get(i).getChildComponents()[0]);
            }
        }
    }

	// exports
	ns.SelectionStrategy = SelectionStrategy;

	return ns;
})(toolbar);