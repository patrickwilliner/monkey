var toolbar = (function(ns) {
    "use strict";

	function CircleStrategy() {	
	}

	// set up inheritance
	CircleStrategy.prototype = new ns.Strategy();
	CircleStrategy.prototype.constructor = CircleStrategy;

	CircleStrategy.prototype.onMouseClick = function(scope, x, y) {
		var circle = new draw.Circle(x, y, 50);

        circle.setBackgroundColor(scope.bgColor);
		scope.shapeList.push(circle);
		scope.render();
	};

	// exports
	ns.CircleStrategy = CircleStrategy;

	return ns;
})(toolbar);