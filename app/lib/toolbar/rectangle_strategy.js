var toolbar = (function(ns) {
    "use strict";

	function RectangleStrategy() {	
	}

	// set up inheritance
	RectangleStrategy.prototype = new ns.Strategy();
	RectangleStrategy.prototype.constructor = RectangleStrategy;

	RectangleStrategy.prototype.onMouseClick = function(scope, x, y) {
		var rectangle = new draw.Rectangle(x - 50, y - 50, 100, 100);

        rectangle.setBackgroundColor(scope.bgColor);
		scope.shapeList.push(rectangle);
		scope.render();
	}

	// exports
	ns.RectangleStrategy = RectangleStrategy;

	return ns;
})(toolbar);