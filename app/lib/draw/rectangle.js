var draw = (function(ns) {
    "use strict";

	function Rectangle(x, y, h, w) {
        ns.Container.call(this);

		this.x = x;
		this.y = y;
		this.h = h;
		this.w = w;
	}

	// set up inheritance
	Rectangle.prototype = new ns.Container();
	Rectangle.prototype.constructor = Rectangle;

	Rectangle.prototype.getBoundingBox = function() {
		return new draw.BoundingBox(this.x, this.y, this.h, this.w);
	};

	Rectangle.prototype.paint = function(svgParent) {
		var rect = svgParent.append('rect');

		rect.attr('x', this.x);
		rect.attr('y', this.y);
		rect.attr('height', this.h);
		rect.attr('width', this.w);
		rect.attr('fill', this.getBackgroundColor());
		rect.attr('stroke', '#000');

		return rect;
	};

    Rectangle.prototype.translate = function(dx, dy) {
        this.x += dx;
        this.y += dy;
    };

    Rectangle.prototype.marshall = function() {
        var simpleObj = {};
        simpleObj.type = 'Rectangle';
        simpleObj.x = this.x;
        simpleObj.y = this.y;
        simpleObj.h = this.h;
        simpleObj.w = this.w;
        simpleObj.backgroundColor = this.getBackgroundColor();
        return simpleObj;
    };

    Rectangle.prototype.unmarshall = function(o) {
        var rectangle = new Rectangle(o.x, o.y, o.h, o.w);
        rectangle.setBackgroundColor(o.backgroundColor);
        return rectangle;
    };

	// exports
	ns.Rectangle = Rectangle;

	return ns;
})(draw);