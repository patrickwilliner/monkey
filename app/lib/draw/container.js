var draw = (function(ns) {
    "use strict";

	function Container() {
        ns.Component.call(this);

        this.childComponents = [];
	}

	// set up inheritance
	Container.prototype = new ns.Component();
	Container.prototype.constructor = Container;

	Container.prototype.addComponent = function(component) {
        this.childComponents.push(component);
	}

    Container.prototype.removeComponent = function(component) {
        // TODO implement
    }

    Container.prototype.getChildComponents = function() {
        return this.childComponents;
    }

    Container.prototype.paintComponent = function (svgParent) {
        var i;

        for (i = 0; i < this.childComponents.length; i++) {
            this.childComponents[i].paintComponent(svgParent);
        }
        return this.paint(svgParent);
    };

    Container.prototype.translate = function(dx, dy) {
        var i;

        for (i = 0; i < this.childComponents.length; i++) {
            this.childComponents[i].translate(dx, dy);
        }
    }

    Container.prototype.setBackgroundColor = function (color) {
        var i;

        ns.Component.prototype.setBackgroundColor.call(this, color);

        for (i = 0; i < this.childComponents.length; i++) {
            this.childComponents[i].setBackgroundColor(color);
        }
    };

	// exports
	ns.Container = Container;

	return ns;
})(draw);