var draw = (function(ns) {
    "use strict";

	function Component() {
        this.backgroundColor = '#ffff00';
	}

    Component.prototype.getBoundingBox = function() {
    };

	Component.prototype.paint = function(svgParent) {
	};

    Component.prototype.contains = function(x, y) {
        var boundingBox = this.getBoundingBox();

        return x > boundingBox.x && x < boundingBox.x + boundingBox.w &&
               y > boundingBox.y && y < boundingBox.y + boundingBox.h;
    };

    Component.prototype.translate = function(dx, dy) {
    };

    Component.prototype.setBackgroundColor = function(color) {
        this.backgroundColor = color;
    };

    Component.prototype.getBackgroundColor = function() {
        return this.backgroundColor;
    };

    Component.prototype.marshall = function() {
    };

    Component.prototype.unmarshall = function(o) {
    };

	ns.Component = Component;

	return ns;
})(draw);