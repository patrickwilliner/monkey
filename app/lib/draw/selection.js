var draw = (function(ns) {
    "use strict";

	function Selection(x, y, h, w) {
        ns.Container.call(this);

		this.x = x;
		this.y = y;
		this.h = h;
		this.w = w;
	}

	// set up inheritance
	Selection.prototype = new ns.Container();
	Selection.prototype.constructor = Selection;

    Selection.prototype.translate = function(dx, dy) {
        ns.Container.prototype.translate.call(this, dx, dy);
        this.x += dx;
        this.y += dy;
    };

	Selection.prototype.getBoundingBox = function() {
		return new draw.BoundingBox(this.x, this.y, this.h, this.w);
	};

	Selection.prototype.paint = function(svgParent) {
		var g = svgParent.append('g'),
            rect;

		// ul
		rect = g.append('rect');
		rect.attr('x', this.x - 2);
		rect.attr('y', this.y - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		// uc
		rect = g.append('rect');
		rect.attr('x', this.x - 2 + this.w / 2);
		rect.attr('y', this.y - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		// ur
		rect = g.append('rect');
		rect.attr('x', this.x + this.w - 2);
		rect.attr('y', this.y - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		// cr
		rect = g.append('rect');
		rect.attr('x', this.x + this.w - 2);
		rect.attr('y', this.y + this.h / 2 - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		// br
		rect = g.append('rect');
		rect.attr('x', this.x + this.w - 2);
		rect.attr('y', this.y + this.h - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		// bc
		rect = g.append('rect');
		rect.attr('x', this.x + this.w / 2 - 2);
		rect.attr('y', this.y + this.h - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		// bl
		rect = g.append('rect');
		rect.attr('x', this.x - 2);
		rect.attr('y', this.y + this.h - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		// cl
		rect = g.append('rect');
		rect.attr('x', this.x - 2);
		rect.attr('y', this.y + this.h / 2 - 2);
		rect.attr('width', 5);
		rect.attr('height', 5);
		rect.attr('fill', '#ffffff');
		rect.attr('stroke', '#000');

		return g;
	};

    Selection.prototype.marshall = function() {
        return this.getChildComponents()[0].marshall();
    };

	// exports
	ns.Selection = Selection;

	return ns;
})(draw);