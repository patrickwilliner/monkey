
var draw = (function(ns) {
    "use strict";


	function Circle(cx, cy, r) {
        ns.Container.call(this);

		this.cx = cx;
		this.cy = cy;
		this.r = r;
	}

	// set up inheritance
	Circle.prototype = new ns.Container();
	Circle.prototype.constructor = Circle;

	Circle.prototype.getBoundingBox = function() {
		return new draw.BoundingBox(this.cx - this.r, this.cy - this.r, 2 * this.r, 2 * this.r);
	};

	Circle.prototype.paint = function(svgParent) {
		var circle = svgParent.append('circle');

		circle.attr('cx', this.cx);
		circle.attr('cy', this.cy);
		circle.attr('r', this.r);
		circle.attr('fill', this.getBackgroundColor());
		circle.attr('stroke', '#000');

		return circle;
	};

    Circle.prototype.translate = function(dx, dy) {
        this.cx += dx;
        this.cy += dy;
    };

    Circle.prototype.marshall = function() {
        var simpleObj = {};
        simpleObj.type = 'Circle';
        simpleObj.cx = this.cx;
        simpleObj.cy = this.cy;
        simpleObj.r = this.r;
        simpleObj.backgroundColor = this.getBackgroundColor();
        return simpleObj;
    };

    Circle.prototype.unmarshall = function(o) {
        var circle = new Circle(o.cx, o.cy, o.r);
        circle.setBackgroundColor(o.backgroundColor);
        return circle;
    };

	// exports
	ns.Circle = Circle;

	return ns;
})(draw);