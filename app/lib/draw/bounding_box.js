var draw = (function(ns) {
    "use strict";

	function BoundingBox(x, y, h, w) {
		this.x = x;
		this.y = y;
		this.h = h;
		this.w = w;
	}

	// exports
	ns.BoundingBox = BoundingBox;

	return ns;
})(draw);