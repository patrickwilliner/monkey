var model = (function(ns) {
    "use strict";

    function List() {
        this.elements = [];
    }

    List.prototype.get = function(idx) {
        return this.elements[idx];
    };

    List.prototype.set = function(idx, value) {
        this.elements[idx] = value;
    };

    List.prototype.push = function(value) {
        this.elements.push(value);
    };

    List.prototype.size = function() {
        return this.elements.length;
    };

    List.prototype.getAll = function() {
        return this.elements;
    };

    // exports
    ns.List = List;

    return ns;
})(model);