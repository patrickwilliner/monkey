var model = (function(ns) {
    "use strict";

    function Iterator() {
    }

    Iterator.prototype.hasNext = function() {
    };

    Iterator.prototype.next = function() {
    };

    // exports
    ns.Iterator = Iterator;

    return ns;
})(model);